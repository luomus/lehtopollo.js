const express = require("express");
const bodyParser = require("body-parser");
const basicAuth = require("express-basic-auth");
const loki = require("lokijs");
const fs = require("fs");
const path = require("path");
const {EOL} = require("os");

const DATA_PATH = "data";
const PASSWORD_PATH = "password";

const PORT = process.env.LEHTOPOLLO_PORT || 3004;
const {LEHTOPOLLO_DEBUG} = process.env;

const app = express();


let items;
const db = new loki("database.json", {
	autoload: true,
	autosave: true,
	autoloadCallback: () => {
		items = db.getCollection("items") || db.addCollection("items");
	},
});

const hitboxes = JSON.parse(new Buffer(fs.readFileSync(path.join(DATA_PATH, "hitboxes.json"))).toString());
const datasets = fs.readdirSync(DATA_PATH).filter(node => fs.lstatSync(path.join(DATA_PATH, node)).isDirectory()).map((folder, idx) => 
	fs.readdirSync(path.join(DATA_PATH, folder)).map(fileName => {
		return {
			idx,
			img: new Buffer(fs.readFileSync(path.join(DATA_PATH, folder, fileName))).toString("base64"),
			hitbox: hitboxes[folder][fileName.replace(/\.[^.]*$/, "")]
		};
	})
);

function randomInt(max) {
	return Math.floor(Math.random() * max);
}

function getByEmailAndId(email, id) {
	return items.findOne({email, $loki: id});
}

const emailIsValid = email => 
	email &&
	email.match(/.+@.+\..+/);

const itemIsValid = (item) => [
	[item => item, "Invalid item"],
	[item => emailIsValid(item.email), "Invalid email"],
	[item => Array.isArray(item.times), "times is not an array"],
	[item => item.times.every(time => typeof time === "number" || time === null), "Invalid time type in times"],
	[item => typeof item.idx === "number", "idx is not a number"],
	[item => item.idx < datasets.length, "idx is not in dataset"],
	[item => item.times.length === datasets[item.idx].length, "times length and dataset length doesn't match"],
	[item => (!("hobbyist" in item) || item.hobbyist === null || typeof item.hobbyist === "boolean"), "invalid value for hobbyist"],
	[item => (!("age" in item) || item.age === null || typeof item.age === "number"), "invalid value for age"]
].find(([validate, message]) => !validate(item));

app.use("/", express.static("src/static"));
app.use("/feedback", express.static("src/static/feedback.html"));
app.use(bodyParser.json());
LEHTOPOLLO_DEBUG && app.use(function logger(req, res, next) {
	const realSend = res.send;
	res.send = function(message) {
		console.log(`REQUEST: ${req.method} ${req.originalUrl} ${new Date()}`);
		if (Object.keys(req.body).length) {
			console.log("BODY");
			const {timeMap, ...body} = req.body;
			console.log(JSON.stringify(body));
			console.log(timeMap);
		}
		console.log(`RESPONSE: ${res.statusCode} ${typeof message === "string" && message.length < 500 &&  message[0] !== "{" && message[0] !== "[" ? message : "<DATA>"}`);
		realSend.apply(res, arguments);
	};
	next();
});

app.get("/dataset", (req, res) => {
	res.setHeader("Content-Type", "application/json");
	const idx = randomInt(datasets.length);
	res.send(escape(JSON.stringify({idx, dataset: datasets[idx]})));
});

app.post("/", (req, res) => {
	const item = req.body;
	const validation = itemIsValid(item);
	if (validation) {
		return res.status(422).send(validation[1]);
	}
	item.date = new Date().toISOString();
	items.insert(item);
	res.status(200).send(JSON.stringify(item.$loki));
});

app.get("/validate-email", (req, res) => {
	const email = req.query.email;
	emailIsValid(email) ? res.sendStatus(200) : res.status(400).send("Virheellinen sähköpostiosoite / Invalid email address");
});

app.post("/feedback", (req, res) => {
	const email = req.query.email;
	const id = +req.query.id;
	if (!email || isNaN(id) || typeof id !== "number") {
		return res.sendStatus(400);
	}
	const item = getByEmailAndId(email, id);
	if (!item) {
		return res.sendStatus(400);
	}
	const feedback = req.body.feedback;
	if (feedback.length > 2000) {
		return res.status(400).send("Palaute ei saa olla yli 2000 merkkiä pitkä / Feedback can't be over 2000 characters long");
	}
	item.feedback = feedback;
	items.update(item);
	res.sendStatus(200);
});

app.use("/error", express.static("src/static/error.html"));
app.post("/error", (req, res) => {
	if (!LEHTOPOLLO_DEBUG) {
		console.log("GOT ERROR");
		console.log(req.body);
	}
	res.sendStatus(200);
});


const password = fs.readFileSync(PASSWORD_PATH).toString().trim();
const adminAuth = basicAuth({
	users: {admin: password},
	challenge: true
});
app.get("/results", adminAuth, (req, res) => {
	function escape(value) {
		if (typeof value !== "string") value = "" + value;
		return (value).replace(/"/g, "\"\"");
	}
	const query = items.find();
	let csv = `"Email","Ikä","Harrastaja","Kuvasarja","Pvm",${datasets[0].map((_, idx) => `"K${idx}"`).join(",")},"Palaute"${EOL}`;
	query.forEach(({email, age, hobbyist, idx, date, feedback, times}) => {
		csv += `"${escape(email)}","${typeof age === "number" ? escape(age) : ""}","${typeof hobbyist === "boolean" ? hobbyist : ""}","${idx + 1}","${date}",${times.map(time => `"${time ? time / 1000 : ""}"`).join(",")},"${feedback ? escape(feedback) : ""}"${EOL}`;
	});
	res.attachment("tulokset.csv").status(200).send(new Buffer(csv));
});

app.listen(PORT, () => console.log(`App listening on port ${PORT}! ${new Date()}`));
