function lehtopolloApp() {
	var TIMEOUT = 60 * 1000;
	var _startTime;
	var container, contentWrapper, logo;

	var utils = lehtopolloUtils(); // eslint-disable-line no-undef

	var _email, _age, _hobbyist, _idx, _times; // copy values for error logging

	function main() {
		container = document.getElementById("container");
		contentWrapper = document.getElementById("content-wrapper");
		logo = document.getElementById("logo");
		getUserInfo(function(email, age, hobbyist) {
			utils.get("/dataset", function(err, response) {
				if (err) return utils.displayError(response || "Tapahtui virhe. Koita myöhemmin uudelleen <p class=\"translation\">An error occurred. Please try again later</p>");
				var jsonResponse = JSON.parse(unescape(response));
				var dataset = jsonResponse.dataset;
				_idx = jsonResponse.idx;
				_startTime = Date.now();
				play(dataset, function(times, timeMap) {
					end({email: email, age: age, hobbyist: hobbyist, idx: jsonResponse.idx, times: times, timeMap: timeMap});
				});
			});
		});
	}

	function getUserInfo(callback) {
		function submitListener(e) {
			document.getElementById("email-form").removeEventListener("submit", submitListener);
			e.preventDefault();
			var email = document.getElementById("email").value.trim();
			_email = email;
			var age = document.getElementById("age").value.trim();
			age = age === "" ? undefined : +age;
			_age = age;
			var hobbyist = document.getElementById("hobbyist").checked;
			_hobbyist = hobbyist;
			utils.get("/validate-email?email=" + email, function(err, response) {
				if (err) {
					return utils.displayError(response);
				}
				callback(email, age, hobbyist);
			});
		}
		document.getElementById("email-form").addEventListener("submit", submitListener);
	}

	function play(dataset, callback) {
		var times = [];
		var timeMap = [];
		_times = times;
		var idx = 0;
		var timeout = undefined;

		function _clearTimeout() {
			if (timeout !== undefined) {
				clearTimeout(timeout);
				timeout = undefined;
			}
		}

		showItem(idx);
		function showItem(idx) {
			var item = dataset[idx];

			var image = new Image();
			image.src = "data:image/jpg;base64," + item.img;
			window.image = image;

			var hitbox = item.hitbox;
			var leftUpper = hitbox[0];
			var rightLower = hitbox[1];

			container.innerHTML = "";
			container.appendChild(image);

			var start = undefined;
			image.onload = function() {
				start = Date.now();
			};

			timeout = setTimeout(function() {
				times.push(null);
				timeMap.push({idx: idx, reason: "TIMEOUT", stamp: Date.now() - _startTime});
				showNavigateToNext();
			}, TIMEOUT);

			function onClick(e) {
				var rect = image.getBoundingClientRect();
				var ratio = getRatio();
				var x = (e.clientX - rect.left) / ratio;
				var y = (e.clientY - rect.top) / ratio;
				if (x >= leftUpper[0] && y >= leftUpper[1] && x < rightLower[0] && y < rightLower[1]) {
					_clearTimeout();
					times.push(Date.now() - start);
					timeMap.push({idx: idx, time: Date.now() - start, reason: "CLICK", stamp: Date.now() - _startTime});
					showNavigateToNext();
				}
			}

			image.addEventListener("click", onClick);

			function getRatio() {
				return image.offsetWidth / image.naturalWidth;
			}

			function showNavigateToNext() {
				_clearTimeout();
				image.removeEventListener("click", onClick);
				var hitboxVisualization = document.createElement("div");
				hitboxVisualization.id = "hitbox-visualization";

				function repaint() {
					var rect = image.getBoundingClientRect();
					var ratio = getRatio();
					hitboxVisualization.style.width = (rightLower[0] - leftUpper[0]) * ratio;
					hitboxVisualization.style.height = (rightLower[1] - leftUpper[1]) * ratio;
					hitboxVisualization.style.left = rect.left + (leftUpper[0] * ratio);
					hitboxVisualization.style.top = rect.top + (leftUpper[1] * ratio);
				}

				repaint();
				container.appendChild(hitboxVisualization);

				function onRepaint() {
					if (requestAnimationFrame) {
						requestAnimationFrame(repaint);
					} else {
						repaint();
					}
				}

				window.addEventListener("resize", onRepaint);
				window.addEventListener("scroll", onRepaint);

				var nextButton = document.createElement("button");
				nextButton.id = "next-button";
				nextButton.innerHTML = "Siirry seuraavaan kuvaan / <i>Show the next photo</i>";
				var buttonContainer = document.createElement("div");
				buttonContainer.id = "next-button-container";
				container.appendChild(buttonContainer);
				buttonContainer.appendChild(nextButton);
				nextButton.addEventListener("click", function() {
					idx++;
					if (idx < dataset.length) {
						window.removeEventListener("resize", onRepaint);
						window.removeEventListener("scroll", onRepaint);
						showItem(idx);
					} else {
						window.removeEventListener("scroll", onRepaint);
						callback(times, timeMap);
					}
				});
			}
		}
	}

	function end(result) {
		var tryAgain = undefined;
		result.browser = (navigator || {}).userAgent;
		utils.postJSON("/", result, function(err, response) {
			if (err) {
				container.innerHTML = "";
				container.appendChild(logo);
				contentWrapper.innerHTML = "";
				container.appendChild(contentWrapper);
				var message = "Tapahtui virhe. Koita uudestaan hetken päästä.<p class=\"translation\">An error occurred. Please try again later.</p>";
				if (response) {
					message += " Virhe: " + response;
				}
				utils.displayError(message);
				if (tryAgain) return;
				tryAgain = document.createElement("button");
				tryAgain.innerHTML = "Koita uudestaan <p class=\"translation\">Try again</p>";
				tryAgain.addEventListener("click", function() {
					return end(result);
				});
				contentWrapper.appendChild(tryAgain);
				return;
			}
			document.cookie = "id=" + response;
			document.cookie = "email=" + result.email;
			document.cookie = "times=" + JSON.stringify(result.times);
			window.location.replace("/feedback");
		});
	}

	window.onerror = function crashGracefully(errorMsg, url, lineNumber) {
		utils.postJSON("/error", {
			errorMsg: errorMsg,
			url: url,
			lineNumber: lineNumber,
			email: _email,
			times: _times,
			idx: _idx,
			age: _age,
			hobbyist: _hobbyist,
			browser: (navigator || {}).userAgent
		}, function() {
			window.location.replace("/error");
		});
	};

	main();
}

window.onload = lehtopolloApp;
