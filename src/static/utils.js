function lehtopolloUtils() {
	var container = document.getElementById("container");
	var contentWrapper = document.getElementById("content-wrapper");
	var messageElem = undefined;

	function displayMessage(message) {
		if (messageElem && container.contains(messageElem)) {
			contentWrapper.removeChild(messageElem);
		}
		messageElem = document.createElement("span");
		messageElem.id = "message";
		messageElem.innerHTML = message;
		contentWrapper.appendChild(messageElem);
		return messageElem;
	}

	function displayError(error) {
		var elem = displayMessage(error);
		elem.className = "error";
	}

	function displaySuccess(message) {
		var elem = displayMessage(message);
		elem.className = "success";
	}

	function postJSON(url, data, callback) {
		var xhr = initializeRequest("POST", url, callback);
		xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
		xhr.send(JSON.stringify(data));
	}

	function get(url, callback) {
		var xhr = initializeRequest("GET", url, callback);
		xhr.send();
	}

	function initializeRequest(type, url, callback) {
		var xhr = new XMLHttpRequest();
		xhr.open(type, url, true);
		xhr.onreadystatechange = function() {
			var status = xhr.status;
			if (xhr.readyState === 4) {
				if (status === 200) {
					callback(null, xhr.response);
				} else {
					callback(status, xhr.response);
				}
			}
		};
		return xhr;
	}

	return {
		displayError: displayError,
		displaySuccess: displaySuccess,
		postJSON: postJSON,
		get: get
	};
}
