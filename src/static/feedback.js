function app() {
	function getCookieValue(key) {
		var value = undefined;
		document.cookie.split(";").some(function(cookie) {
			var splitted = cookie.split("=");
			if (splitted[0].trim() === key) {
				value = splitted[1];
				return true;
			}
		});
		return value;
	}

	var utils = lehtopolloUtils(); // eslint-disable-line no-undef

	var feedbackInput = document.getElementById("feedback-input");

	var times = undefined;
	var email = getCookieValue("email");
	var id = +getCookieValue("id");
	try {
		times = JSON.parse(getCookieValue("times"));
	} catch (e) {
		if (!email || isNaN(id) || typeof id !== "number") {
			document.getElementById("content-wrapper").innerHTML = "";
			utils.displayError("Jotain meni pieleen. Oletko vastannut kyselyyn ennen tälle sivulle tuloa? <span class=\"translations\">Something went wrong. have you answered the survey before navigating to this page?</span> <a href=\"/\">Palaa kyselyyn / <span class=\"translation\">Return to the survey</span></a>");
			return;
		}
	}
	var found = times.filter(function(time) {return time;});
	var totalTime = found.reduce(function(total, time) {
		return total + time;
	}, 0) / 1000;

	var howManyFound = document.getElementById("how-many-found");
	var meanTime = found.length ? Math.round(totalTime / found.length * 10) / 10 : 0;
	howManyFound.innerHTML =
		"Löysit " + found.length + " / " + times.length + " pöllöstä (aikaa " + meanTime + " sekuntia / kuva).\n" +
		"<p class=\"translation\">You found " + found.length + " / " + times.length + " owls (time " + meanTime + " sec / photo).</p>";

	document.getElementById("feedback-form").addEventListener("submit", function(e) {
		e.preventDefault();
		utils.postJSON("/feedback?email=" + email + "&id=" + id, {feedback: feedbackInput.value}, function(err, response) {
			if (err) return utils.displayError(response || "Palautteen lähettäminen epäonnistui <p class=\"translation\">Sending feedback failed</p>");
			utils.displaySuccess("Kiitos palautteesta! <p class=\"translation\">Thank you for the feedback!</p>");
		});
	});
}

window.onload = app;
